ALTER TABLE  `ocenter_module` ADD  `auth_role` VARCHAR( 200 ) NOT NULL COMMENT  '允许访问角色，为空表示非登录也能访问';

INSERT INTO `ocenter_menu`(`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
('自定义表情列表', 74, 0, 'Expression/iexpressionList', 0, '', '表情设置', 0, '', '');

CREATE TABLE IF NOT EXISTS `ocenter_weibo_topic_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `ocenter_menu`(`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
( '允许身份访问', 116, 0, 'Role/moduleRole', 0, '', '模块权限', 0, '', '');